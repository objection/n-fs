#pragma once
#include <stdbool.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <wait.h>
#include <sys/socket.h>

struct n_fs_dirstack {
	char **dirs;
	size_t n_dirs;
	size_t a_dirs;
};

struct n_fs_fd_pair {
	int fds[2];
	bool failed;
};

int n_fs_systemf (char *fmt, ...);
char *n_fs_get_full_real_path (char *abbreviated_path);
char *n_fs_get_full_pathf (char *path_fmt, ...);
char *n_fs_get_full_path (const char *path);
/* int n_fs_pushd (struct dirstack *dirs, char *dir); */
/* int n_fs_popd (struct dirstack *dirs); */
bool n_fs_fexists (char *file);
int n_fs_exec_list (bool use_shell, char **list, char **env);
int n_fs_exec_nowait (bool use_shell, char *cmd, char **env);
int n_fs_exec_list_nowait (bool use_shell, char **list, char **env);
int n_fs_mkdirf (mode_t mode, char *fmt, ...);
int n_fs_mkdir_pf (mode_t mode, char *fmt, ...);
int n_fs_mkdir_if_not_exist (char *path, mode_t mode);
int n_fs_mkdir_p (const char *path, mode_t mode);
int n_fs_renamef (char *file, char *fmt, ...);
int n_fs_rm_rf (char *path);
bool n_fs_is_possibly_tty (void);
ssize_t n_fs_copy_fd_traditionally (int old_fd, int new_fd);
int n_fs_copy_fd (int old_fd, int new_fd);
int n_fs_copy_file (char *old_path, char *new_path, bool no_clobber);
int n_fs_mv (char *old_path, char *new_path, bool no_clobber);
int n_fs_cp (char *old_path, char *new_path, bool no_clobber);
ssize_t n_fs_write (int fd, const void *buf, size_t size);
ssize_t n_fs_read (int fd, void *buf, size_t size);
char *n_fs_read_all (size_t *n_r, int fd);
int n_fs_spurt (char *buf, char *path);
int n_fs_spurt_fmt (char *path, char *fmt, ...);
char *n_fs_chdir (char *path);
char *n_fs_make_rel_path (char *path, char *dir);
int n_fs_dputc (char c, int fd);
int n_fs_dputchar (char c);
int n_fs_newline_f (FILE *f);
int n_fs_newline_fd (int fd);
char *n_fs_exepath (const char *prog);
struct n_fs_fd_pair n_fs_pipe ();
char *n_fs_waitid_strerror (const siginfo_t *siginfo);
int n_fs_connect_with_timeout (int sockfd, const struct sockaddr *addr, socklen_t addrlen,
		unsigned int timeout_ms);
int n_fs_fexists_and_is (char *file_path, mode_t mode, int fstatat_flags, bool *out);
