#define _GNU_SOURCE
#include <stdarg.h>
#include <stdlib.h>
#include <sys/sendfile.h>
#include <assert.h>
#include <poll.h>
#include <sys/types.h>
#include <dirent.h>
#include <time.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <wordexp.h>
#include <unistd.h>
#include <ftw.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "n-fs.h"

#define arr_push(v, val) \
  ({ n_fs_arr_expand_(arr_unpack_(v)) ? -1 :\
    ((v)->d[(v)->n++] = (val), 0), 0; })

#define arr_pop(v) \
  (v)->d[--(v)->n]

#define arr_unpack_(v) \
  (char**)&(v)->d, &(v)->n, &(v)->a, sizeof(*(v)->d)

#define $make_arr(T, name) \
  struct name { T *d; size_t n, a; }

#if 0
static int n_fs_arr_expand_ (char **d, size_t *n, size_t *a, int memsz) {
  if (*n + 1 > *a) {
    void *ptr;
    int new_a = (*a == 0) ? 1 : *a << 1;
	if (!*d)
		ptr = malloc (new_a * memsz);
	else
		ptr = realloc (*d, new_a * memsz);
    if (ptr == NULL)
		return -1;
    *d = ptr;
    *a = new_a;
  }
  return 0;
}
#endif

static void make_list_and_exec (bool use_shell, char **list,
		char **env) {
	if (use_shell) {
		int len = ({
			len = 0;
			for (char **_ = list; *_; _++)
				len += strlen (*_) + 1;
			len + 1;
		});
		char *arg = calloc (len, sizeof *arg);
		char *p = arg;
		for (char **_ = list; *_; _++) {
			p = stpcpy (p, *_);
			*p++ = ' ';
		}
		execvpe ("sh", (char *[]) {"sh", "-c", arg, 0}, env);
	} else
		execvpe (list[0], list, env);
}

int n_fs_exec_list (bool use_shell, char **list, char **env) {
	pid_t pid = fork ();
	if (pid == -1) return 1;
	if (pid == 0)  make_list_and_exec (use_shell, list, env);
	else {
		/* int wstatus; */
		/* int rt = wait (&wstatus); */

		/* // This is a bodge for now. */
		/* if (rt || (WIFEXITED (wstatus) && WEXITSTATUS (wstatus) != 0)) */
		/* 	return 1; */
	}
	int wstatus;
	waitpid (pid, &wstatus, 0);
	return wstatus;
}

int n_fs_exec_list_nowait (bool use_shell, char **list, char **env) {
	pid_t pid = fork ();
	if (pid == -1)
		return 1;
	if (pid == 0)  make_list_and_exec (use_shell, list, env);
	else {
		/* int wstatus; */
		/* int rt = wait (&wstatus); */

		/* // This is a bodge for now. */
		/* if (rt || (WIFEXITED (wstatus) && WEXITSTATUS (wstatus) != 0)) */
		/* 	return 1; */
	}
	return 0;
}

int n_fs_exec_nowait (bool use_shell, char *cmd, char **env) {
	pid_t pid = fork ();
	if (pid == -1)
		return 1;
	if (pid == 0)  {
		if (use_shell)
			execvpe ("sh", (char *[]) {"sh", "-c", cmd, 0}, env);
		else {
			wordexp_t p;
			int rt = wordexp (cmd, &p, WRDE_NOCMD | WRDE_UNDEF);
			if (rt) return 1;
			n_fs_exec_list_nowait (use_shell, p.we_wordv, env);
			wordfree (&p);
		}
	} else {
		/* int wstatus; */
		/* int rt = wait (&wstatus); */

		/* // This is a bodge for now. */
		/* if (rt || (WIFEXITED (wstatus) && WEXITSTATUS (wstatus) != 0)) */
		/* 	return 1; */
	}
	return 0;
}


int n_fs_systemf (char *fmt, ...) {
	assert (fmt);
	va_list ap;
	va_start (ap, fmt);
	char *str = NULL;
	vasprintf (&str, fmt, ap);
	va_end (ap);

	// Presumably you've run out of memory, right?
	assert (str);
	int system_ret = system (str);
	free (str);
	return system_ret;
}

// This gets the string; it doesn't check if it's valid.
char *n_fs_get_full_path (const char *path) {
	assert (path);
	char *r = NULL;
	if (*path == '~') {
		char *home = getenv ("HOME");
		asprintf (&r, "%s/%s", home, &path[2]);
	} else {
		if (*path == '/')
			r = strdup (path);
		else

			// I was asprintfing a ./ to the start here but I don't
			// think that's necessary
			r = strdup (path);
	}
	return r;
}

char *n_fs_get_full_real_path (char *abbreviated_path) {
	char *full_path = n_fs_get_full_path (abbreviated_path);
	char *r = realpath (full_path, 0);
	free (full_path);
	return r;
}

char *n_fs_get_full_pathf (char *path_fmt, ...) {
	char path[PATH_MAX];
	va_list ap;
	va_start (ap, path_fmt);
	vsnprintf (path, sizeof path, path_fmt, ap);
	char *r = n_fs_get_full_path (path);
	va_end (ap);
	return r;

}

#if 0
int pushd (struct dirstack *dirs, char *dir) {
	if (-1 == chdir (dir))
		return 1;
	arr_push (dirs, dir);
	return 0;
}

int popd (struct dirstack *dirs) {
	arr_pop (dirs);
	if (0 == dirs->n)
		return 1;
	if (-1 == chdir (dirs->d[dirs->n - 1]))
		return 1;
	return 0;
}
#endif

bool fexists (char *file) {
	bool was_enoent = errno == ENOENT;
	int r = access (file, F_OK);

	// This is the right thing to do. It's not right to make an errno
	// vanish unexpectedly.
	if (!was_enoent && errno == ENOENT)
		errno = 0;
	return !r;
}

// This function has a big flaw: it can't relay stat's errors. I don't
	// really want to fix it right now. It's just that situation
	// again: this is a function that's meant to make a common task
	// easier, but since its parts can fail, it turns out there's
	// really no way to make it simpler without ignoring errors. Maybe
	// you could pass some "error condition handler" in? That's an
	// interesting idea. Still, all you could really do in such a
	// thing is longjmp or exit, right? Which might be OK.
int n_fs_fexists_and_is (char *file_path, mode_t mode, int fstatat_flags, bool *out) {
	int rc;
	struct stat sb = {};

	rc = fstatat (AT_FDCWD, file_path, &sb, fstatat_flags);
	if (rc)
		return 1;
	*out = (sb.st_mode & S_IFMT) == mode;
	return 0;
}

int n_fs_mkdir_p (const char *path, mode_t mode) {

	// Stolen from
	// https://gist.github.com/JonathonReinhart/8c0d90191c38af2dcadb102c4e202950
    /* Adapted from http://stackoverflow.com/a/2336245/119527 */
    const size_t len = strlen (path);
    char _path[PATH_MAX];
    char *p;

    errno = 0;

    /* Copy string so its mutable */
    if (len > sizeof (_path) - 1) {
        errno = ENAMETOOLONG;
        return -1;
    }
    strcpy (_path, path);

    /* Iterate the string */
    for (p = _path + 1; *p; p++) {
        if (*p == '/') {

            /* Temporarily truncate */
            *p = '\0';

            if (mkdir (_path, mode) != 0) {
                if (errno != EEXIST)
                    return -1;
            }

            *p = '/';
        }
    }

    if (mkdir (_path, mode) != 0) {
        if (errno != EEXIST)
            return -1;
    }

    return 0;
}

int n_fs_mkdirf (mode_t mode, char *fmt, ...) {
	int r = 0;
	va_list ap;
	va_start (ap, fmt);
	char buf[PATH_MAX];
	vsnprintf (buf, PATH_MAX, fmt, ap);
	if (mkdir (buf, mode))
		r = -1;
	va_end (ap);
	return r;
}

int n_fs_mkdir_pf (mode_t mode, char *fmt, ...) {
	int r = 0;
	va_list ap;
	va_start (ap, fmt);
	char buf[PATH_MAX];
	vsnprintf (buf, PATH_MAX, fmt, ap);
	if (n_fs_mkdir_p (buf, mode))
		r = -1;
	va_end (ap);
	return r;
}

// Not much point in this function, really. You still have to check
// errno.
//
// 		if (mkdir () && errno != EEXIST)
// 			err (...)
//
// 		if (mkdir_if_not_exist ())
// 			err (...)
// Eh. Really we're doing some extra branching, and I wouldn't even
// say things are clearer. You'll need to use errno in your error
// message anyway.
int n_fs_mkdir_if_not_exist (char *path, mode_t mode) {
	errno = 0;
	int rt = mkdir (path, mode);
	if (errno == EEXIST)
		errno = 0;
	if (rt && errno)
		return rt;
	return 0;
}

// Returns the number of bytes written; -1 on failure.
ssize_t n_fs_copy_fd_traditionally (int old_fd, int new_fd) {

	// Bufsize; why not
	char buf[BUFSIZ];
	ssize_t bytes_read;
	ssize_t bytes_written;

	ssize_t r = 0;
	errno = 0;
	while ((bytes_read = read (old_fd, buf, BUFSIZ)) > 0) {
		if (bytes_read == -1)
			return -1;
		while (bytes_read > 0) {
			bytes_written = write (new_fd, buf, bytes_read);
			r += bytes_written;
			if (bytes_written == -1)
				return -1;
			bytes_read -= bytes_written;
		}
	}
	return r;
}

// Tries to use copy_file_range, and falls back to
// n_fs_copy_fd_traditionally when
// that fails (which it seems pretty likely to do).
//
// Returns the number of bytes written; -1 on failure.
int n_fs_copy_fd (int old_fd, int new_fd) {
	struct stat old_fd_sb;
	if (fstat (old_fd, &old_fd_sb))
		return 1;

	ssize_t r = copy_file_range (old_fd, 0, new_fd, 0, -1, 0);
	if (r != old_fd_sb.st_size) {

		// The manpage says you can use copy_file_range across
		// filesystems, but you can't, not reliably anymore. So we
		// fall back to this. 2023-09-26T13:59:23+01:00: When I said
		// "this", before, I meant n_fs_copy_fd_traditionally, but
		// that doesn't work. Maybe the function is bad. sendfile
		// works. Another thought: if we're using sendfile we might as
		// well just use sendfile, since it seems to be much the same
		// thing. There's also "spice".
		if (errno == EXDEV) {
			errno = 0;
			int total_bytes_sent = 0;
			for (;;) {
				int bytes_sent = sendfile (new_fd, old_fd, 0, old_fd_sb.st_size);
				if (bytes_sent == -1)
					return -1;
				total_bytes_sent += bytes_sent;
				if (total_bytes_sent >= old_fd_sb.st_size)
					break;
			}
			return total_bytes_sent;
			/* r = n_fs_copy_fd_traditionally (old_fd, new_fd); */
			/* if (r != old_fd_sb.st_size) */
			/* 	return -1; */
			/* return r; */
		}
		return -1;
	}
	if (ftruncate (new_fd, r))
		return -1;
	return r;
}

bool n_fs_fexists (char *file) {
	bool was_enoent = errno == ENOENT;
	int r = access (file, F_OK);

	// This is the right thing to do. It's not right to make an errno
	// vanish unexpectedly.
	if (!was_enoent && errno == ENOENT)
		errno = 0;
	return !r;
}

int n_fs_copy_file (char *old_path, char *new_path, bool no_clobber) {
	if (no_clobber && n_fs_fexists (new_path))
		return -1;

	int old_fd = open (old_path, O_RDONLY);
	if (old_fd == -1)
		return -1;

	struct stat old_fd_sb;
	int rt = fstat (old_fd, &old_fd_sb);
	if (rt)
		return -1;

	int new_fd = open (new_path, O_WRONLY | O_CREAT | O_TRUNC, old_fd_sb.st_mode & 0777);
	if (new_fd == -1)
		return -1;

	int r = n_fs_copy_fd (old_fd, new_fd);

	// If we can't close the fds, what? An error? Obviously, I
	// suppose.
	if (close (old_fd) == -1)
		return -1;
	if (close (new_fd) == -1)
		return -1;

	return r;
}

// See the n_fs_mv explanation; this is the same sort of thing.
int n_fs_cp (char *old_path, char *new_path, bool no_clobber) {
	struct stat new_path_sb;
	int rt = lstat (new_path, &new_path_sb);
	char buf[PATH_MAX];
	if (rt) {
		if (no_clobber && errno == EEXIST && !S_ISDIR (new_path_sb.st_mode))
			return rt;
	}

	if (S_ISDIR (new_path_sb.st_mode)) {
		// Could check for symlink?

		snprintf (buf, PATH_MAX, "%s/%s", new_path, basename (old_path));
		new_path = buf;
	}
	rt = n_fs_copy_file (old_path, new_path, no_clobber);
	if (rt == -1)
		return 1;
	return 0;
}


// Simulates the mv command, in that if new_path is the name of
// a directory it will move old_path into it. n_fs_rename will also
// move across filesystems by copying the file and removing the
// original. With no_clobber it will refuse to overwrite new_path.
// You can get a noclobber with renameat2, btw. Though doesn't that
// function seem a complete pain to use? The point of this function,
// and most function in this library, is to be convenient. So it's not
// bad. It'll save you a few lines.
//
// Uses lstat -- ie, renames symlinks rather than the things they
// point to -- for now anyway.
//
// Returns 0 or 1 because you're not meant to care about the file size
// when you use this function. If you care about the filesize, do it
// the long way.
int n_fs_mv (char *old_path, char *new_path, bool no_clobber) {
	struct stat new_path_sb;
	int rt = lstat (new_path, &new_path_sb);
	char buf[PATH_MAX];
	if (rt) {
		if (no_clobber && errno == EEXIST && !S_ISDIR (new_path_sb.st_mode))
			return rt;
	}

	if (S_ISDIR (new_path_sb.st_mode)) {
		// Could check for symlink?

		snprintf (buf, PATH_MAX, "%s/%s", new_path, basename (old_path));
		new_path = buf;
	}

	rt = rename (old_path, new_path);
	if (rt) {

		// I didn't realise how much work it would be to copy a
		// directory tree. So here we only copy the directory if it's
		// a file. Pathetic. But I'm not spending the time necessary
		// to write a proper directory-moving function.
		if (errno == EXDEV && S_ISREG (new_path_sb.st_mode)) {
			rt = n_fs_copy_file (old_path, new_path, no_clobber);
			if (rt == -1)
				return 1;
			remove (old_path);
		} else
			return 1;
	}
	return 0;
}

char *n_fs_waitid_strerror (const siginfo_t *siginfo) {

	static char r[BUFSIZ];
#define ADD_IT($fmt, ...) snprintf (r, sizeof r, $fmt, ##__VA_ARGS__)
	switch (siginfo->si_code) {
	case CLD_EXITED:  assert (false && "Don't call this if the thing didn't fail"); break;
	case CLD_KILLED:  ADD_IT ("Killed by signal %s", strsignal (siginfo->si_status)); break;
	case CLD_DUMPED:  ADD_IT ("Dumped core because of signal %s", strsignal (siginfo->si_status)); break;
	case CLD_STOPPED: ADD_IT ("Stopped by signal %s", strsignal (siginfo->si_status)); break;
	default:          assert (false);
	}
	return r;
}

int n_fs_renamef (char *old_path, char *fmt, ...) {
	int r = 0;
	va_list ap;
	va_start (ap, fmt);
	char buf[PATH_MAX];
	vsnprintf (buf, PATH_MAX, fmt, ap);
	r = rename (old_path, buf);
	va_end (ap);
	return r;
}

int unlink_cb (const char *fpath, const struct stat *sb, int typeflag,
		struct FTW *ftwbuf) {
	(void) sb; (void) typeflag; (void) ftwbuf;
    int rv = remove (fpath);
    return rv;
}

int n_fs_rm_rf (char *path) {
    return nftw (path, unlink_cb, 64, FTW_DEPTH | FTW_PHYS);
}

bool n_fs_is_possibly_tty (void) {

	// Ripped brutally from grep source code.
	// Haven't even tried to work out how it works.
#define SAME_INODE(a, b)    \
    ((a).st_ino == (b).st_ino \
     && (a).st_dev == (b).st_dev)
	struct stat tmp_stat;
	if (fstat (STDOUT_FILENO, &tmp_stat) == 0)
	{
		if (S_ISCHR (tmp_stat.st_mode))
		{
			struct stat null_stat;
			if (stat ("/dev/null", &null_stat) == 0
					&& SAME_INODE (tmp_stat, null_stat));
			/* dev_null_output = true; */
			else
				return 1;
		}
	}
	return 0;
#undef SAME_INODE
}

ssize_t n_fs_write (int fd, const void *buf, size_t size) {
	int r = 0;
	while (size) {
		ssize_t bytes_written = write (fd, buf, size);
		if (bytes_written < 0 && errno == EINTR) continue;
		r += bytes_written;
		if (bytes_written <= 0) {
			if (errno)
				return -1;
			else
				break;
		}
		buf = (const char *) buf + bytes_written;
		size -= bytes_written;
	}
	return r;
}

ssize_t n_fs_read (int fd, void *buf, size_t size) {
	int r = 0;
	while (size) {
		ssize_t bytes_read = read (fd, buf, size);
		if (bytes_read < 0 && errno == EINTR) continue;
		r += bytes_read;
		if (bytes_read <= 0) {
			if (errno)
				return -1;
			else
				break;
		}
		buf = (char *) buf + bytes_read;
		size -= bytes_read;
	}
	return r;
}


char *n_fs_read_all (size_t *n_r, int fd) {
	char *r;
	FILE *f = open_memstream (&r, n_r);
	if (!f) return 0;

	char buf[BUFSIZ];
	ssize_t size;
	while ((size = n_fs_read (fd, buf, BUFSIZ)) > 0)
		fwrite (buf, size, 1, f);

	if (size == -1 && errno) {
		r = 0;
		goto out;
	}

out:
	fclose (f);

	if (!*r) {
		free (r);
		r = 0;
	}
	return r;
}

int n_fs_spurt (char *buf, char *path) {
	FILE *f = fopen (path, "w+");
	if (!f)
		return -1;
	int r = fwrite (buf, 1, strlen (buf), f);
	if (fclose (f))
		return -1;
	return r;
}

int n_fs_spurt_fmt (char *path, char *fmt, ...) {

	va_list ap;
	va_start (ap);
	char *buf = 0;

	vasprintf (&buf, fmt, ap);
	va_end (ap);
	n_fs_spurt (path, buf);
	return 0;
}

// Chdirs, returing your original dir (statically allocated).
char *n_fs_chdir (char *path) {
	static char orig[PATH_MAX];
	if (!getcwd (orig, PATH_MAX))
		return 0;
	if (chdir (path))
		return 0;
	return orig;
}


char *n_fs_make_rel_path (char *path, char *dir) {
	char dir_buf[PATH_MAX], path_buf[PATH_MAX];
	dir = dir ? realpath (dir, dir_buf) : getcwd (dir, sizeof dir_buf);

	// Always realpath so we have a proper string to work with -- no
	// repeated slashes, eg.
	path = realpath (path, path_buf);
	if (!dir[1])
		return strdup (path);

	char *r = 0; size_t n_r = 0;
	FILE *f = open_memstream (&r, &n_r);
	if (!f) return r;

	char *head = strchr (dir, 0);
	bool first = 1;
	for (;;) {
		int len = head - dir;
		if (!strncmp (path, dir, len)) {
			if (first)
				fprintf (f, "./%s", path + len + 1);
			else
				fprintf (f, "%s", path + len + 1);

			break;
		} else {
			fprintf (f, "../");
		}
		if (head == dir)
			break;
		head--;
		for (; *head != '/'; head--) {
		}
		first = 0;
	}

	fclose (f);
	return r;
}

// fputc returns its argument cast to an int. I won't do that unless I
// can see a reason to do it.
int n_fs_dputc (char c, int fd) {
	return write (fd, (char []) {c}, sizeof c);
}

int n_fs_dputchar (char c) {
	return write (STDOUT_FILENO, (char []) {c}, sizeof c);
}

int n_fs_newline_fd (int fd) {
	return write (fd, (char []) {'\n'}, sizeof (char));
}

int n_fs_newline_f (FILE *f) {
	return fputc ('\n', f);
}

char *n_fs_exepath (const char *prog) {
	char *r = 0;
	if (!getenv ("PATH"))
		return 0;
	char *path = strdup (getenv ("PATH"));
	char *p = path;
	char *_;
	char buf[PATH_MAX];
	while ((_ = strsep (&p, ":"))) {
		DIR *dp = opendir (_);
		if (!dp)
			continue;
		const struct dirent *dirent;
		struct stat stat_buf;
		while ((dirent = readdir (dp))) {
			snprintf (buf, sizeof buf, "%s/%s", _, dirent->d_name);
			int rc = stat (buf, &stat_buf);
			if (rc)
				continue;

			if ((stat_buf.st_mode & S_IFMT) != S_IFREG)
				continue;

			if (!strcmp (dirent->d_name, prog)) {
				r = strdup (buf);
				closedir (dp);
				goto out;
			}
		}
		closedir (dp);
	}
out:
	free (path);
	return r;
}

// Save yourself a single line by using this, which returns a struct
// with the fds inside it.
struct n_fs_fd_pair n_fs_pipe () {
	struct n_fs_fd_pair r = {.failed = 1};
	int rc = pipe (r.fds);
	if (!rc)
		r.failed = 0;
	return r;
}

// Straight copied from
// https://stackoverflow.com/questions/2597608/c-socket-connection-timeout
int n_fs_connect_with_timeout (int sockfd, const struct sockaddr *addr, socklen_t addrlen,
		unsigned int timeout_ms) {
    int rc = 0;

    // Set O_NONBLOCK
    int sockfd_flags_before;
    if ((sockfd_flags_before = fcntl (sockfd, F_GETFL, 0) < 0))
		return -1;
    if (fcntl (sockfd, F_SETFL, sockfd_flags_before | O_NONBLOCK) < 0)
		return -1;

    // Start connecting (asynchronously)
    do {
        if (connect (sockfd, addr, addrlen) < 0) {

            // Did connect return an error? If so, we'll fail.
            if ((errno != EWOULDBLOCK) && (errno != EINPROGRESS))
                rc = -1;

            // Otherwise, we'll wait for it to complete.
            else {

                // Set a deadline timestamp 'timeout' ms from now (needed b/c poll can be interrupted)
                struct timespec now;
                if (clock_gettime (CLOCK_MONOTONIC, &now) < 0) {
					rc = -1;
					break;
				}
                struct timespec deadline = {
					.tv_sec = now.tv_sec,
        			.tv_nsec = now.tv_nsec + timeout_ms*1000000l
				};

                // Wait for the connection to complete.
                do {

                    // Calculate how long until the deadline
                    if (clock_gettime (CLOCK_MONOTONIC, &now) < 0) {
						rc = -1;
						break;
					}
					int ms_until_deadline = (int) ((deadline.tv_sec - now.tv_sec) * 1000l
							+ (deadline.tv_nsec - now.tv_nsec) / 1000000l);
                    if (ms_until_deadline < 0) {
						rc = 0;
						break;
					}

                    // Wait for connect to complete (or for the timeout deadline)
                    struct pollfd pfds[] = {{.fd = sockfd, .events = POLLOUT}};
                    rc = poll (pfds, 1, ms_until_deadline);

                    // If poll 'succeeded', make sure it *really* succeeded
                    if (rc > 0) {
                        int error = 0;
						socklen_t len = sizeof (error);
                        int retval = getsockopt (sockfd, SOL_SOCKET, SO_ERROR, &error,
								&len);
                        if (retval == 0)
							errno = error;
                        if (error != 0)
							rc = -1;
                    }
                }

                // If poll was interrupted, try again.
                while (rc == -1 && errno == EINTR);

                // Did poll timeout? If so, fail.
                if (rc == 0) {
                    errno = ETIMEDOUT;
                    rc = -1;
                }
            }
        }
    } while (0);

    // Restore original O_NONBLOCK state
    if (fcntl (sockfd, F_SETFL, sockfd_flags_before) < 0)
		return -1;

    // Success
    return rc;
}

